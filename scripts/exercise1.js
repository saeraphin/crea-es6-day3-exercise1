//import Shape from './Shape.js'

(function exercise1() {
  /**
   * Faisons des classes.
   * 
   * 1. Créer une classe 'Shape'
   *  a. dans le fichier 'scripts/Shape.js'
   *  b. l'exporter et l'importer dans le fichier d'exercice (import Shape from './Shape.js')
   *  c. définir la propriété "privée" '_sideCount' et son getter 'sideCount'
   *  d. redéfinir la méthode 'toString()' et lui faire retourner 'Je suis une forme à x côtés.'
   *  e. définir les getters pour 'perimeter' et 'area'
   *  f. par défaut, ces getters devraient retourner NaN (vous pouvez même rajouter un console.warn(...))
   * 
   * 2. Créer une classe 'RectangleShape'
   *  a. dans le fichier 'scripts/RectangleShape.js'
   *  b. l'exporter et l'importer dans le fichier d'exercice
   *  c. faire hériter 'RectangleShape' de 'Shape'
   *  d. dans le constructeur, faire appeler le constructeur parent avec le bon nombre de côtés (super)
   *  e. définir les propriété "privées" '_width' et '_height' et leurs getters 'width' et 'height'
   *  f. définir les getters pour 'perimeter' et 'area'
   *  g. retournez le périmètre et l'aire calculés dans les getters 'perimeter' et 'area'
   *  h. redéfinier la méthode 'toString()' lui faire retourner la valeur du parent (super) plus 'Je suis un rectangle.'
   * 
   * Bonus
   * 1. Créer la classe 'SquareShape'
   *  a. faire hériter 'SquareShape' de 'RectangleShape'
   *  b. utiliser les consignes du step 2
   *  c. la plupart des propriétés du carré sont déjà définie, c'est 'juste' un rectangle à côtés égaux
   * 2. Créer la classe 'CircleShape'
   *  a. faire hériter 'CircleShape' de 'Shape'
   *  b. utiliser les consignes du step 2
   *  c. un cercle a les propriétés 'radius' et 'diameter' (une seule devrait être stockée, l'autre calculée)
   */



  // TODO #1
  console.info(`<Exercise 1. Step 1>`)
  try {
    // TODO #1.a b
    const shape = new Shape(3)
    // TODO #1.c
    console.log(`- shape.sideCount: ${shape.sideCount}`)
    // TODO #1.d
    console.log(`- shape.toString(): "${shape}"`)
    // TODO #1.e f
    console.log(`- shape.perimeter: ${shape.perimeter}`)
    console.log(`- shape.area: ${shape.area}`)
  } catch (error) {
    console.warn('There a still errors in the step 1.')
    console.error(error)
  }
  console.info(`</Exercise 1. Step 1>\n\n\n`)



  // TODO #2
  console.info(`<Exercise 1. Step 2>`)
  try {
    // TODO #2.a b
    const rect = new RectangleShape(20, 10)
    // TODO #1.c d
    console.log(`- rect.sideCount: ${rect.sideCount}`)
    // TODO #1.e
    console.log(`- rect.width: ${rect.width}`)
    console.log(`- rect.height: ${rect.height}`)
    // TODO #1.f g
    console.log(`- rect.perimeter: ${rect.perimeter}`)
    console.log(`- rect.area: ${rect.area}`)
    // TODO #1.h
    console.log(`- rect.toString(): "${rect}"`)
  } catch (error) {
    console.warn('There a still errors in the step 2.')
    console.error(error)
  }
  console.info(`</Exercise 1. Step 2>\n\n\n`)



  // TODO #Bonus1
  console.info(`<Bonus 1>`)
  try {
    const square = new SquareShape(30)
    console.log(`- square.sideCount: ${square.sideCount}`)
    console.log(`- square.width: ${square.width}`)
    console.log(`- square.height: ${square.height}`)
    console.log(`- square.perimeter: ${square.perimeter}`)
    console.log(`- square.area: ${square.area}`)
    console.log(`- square.toString(): "${square}"`)
  } catch (error) {
    console.warn('There a still errors in the bonus 1.')
    console.error(error)
  }
  console.info(`</Bonus 1>\n\n\n`)



  // TODO #Bonus2
  console.info(`<Bonus 2>`)
  try {
    const circle = new CircleShape(10)
    console.log(`- circle.sideCount: ${circle.sideCount}`)
    console.log(`- circle.radius: ${circle.radius}`)
    console.log(`- circle.diameter: ${circle.diameter}`)
    console.log(`- circle.perimeter: ${circle.perimeter}`)
    console.log(`- circle.area: ${circle.area}`)
    console.log(`- circle.toString(): "${circle}"`)
  } catch (error) {
    console.warn('There a still errors in the bonus 2.')
    console.error(error)
  }
  console.info(`</Bonus 2>\n\n\n`)

})()